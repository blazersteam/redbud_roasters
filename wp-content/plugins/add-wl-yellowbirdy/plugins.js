jQuery(document).ready(function() {
	var tr = jQuery('.plugin-update-tr[data-slug="ecwid-shopping-cart"]');
	var href = jQuery('.update-link', tr).attr('href');
	
	jQuery('.update-message p', tr).html(EcwidWLPlugins.message);
	jQuery('.update-message p a.update-link', tr).attr('href', href);
	tr.addClass('ecwid-wl-update');
});
