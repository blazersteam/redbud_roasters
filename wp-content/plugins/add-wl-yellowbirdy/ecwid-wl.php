<?php
/*
Plugin Name: YellowBirdy
Version: 6.4.8
Text Domain: ecwid-shopping-cart
*/

define( 'ECWID_WL_PLUGIN_NAME', 'YellowBirdy' );
define( 'ECWID_WL_PLUGIN_DESCRIPTION', 'YellowBirdy is a free full-featured shopping cart. It can be easily integrated with any Wordpress blog and takes less than 5 minutes to set up.' );

add_filter('all_plugins', 'ecwid_wl_all_plugins');

function ecwid_wl_all_plugins( $plugins )
{
        $key = 'ecwid-shopping-cart/ecwid-shopping-cart.php';
        $ecwid = @$plugins[$key];

        if ($ecwid) {
                $ecwid['Name'] = ECWID_WL_PLUGIN_NAME;
                $ecwid['Description'] = ECWID_WL_PLUGIN_DESCRIPTION;
                $plugins[$key] = $ecwid;
        }
        return $plugins;
}

add_action('admin_enqueue_scripts', 'ecwid_wl_enqueue_scripts');

function ecwid_wl_enqueue_scripts()
{
	wp_enqueue_style( 'ecwid-wl-plugins', plugin_dir_url( __FILE__ ) . 'plugins.css' );
	wp_enqueue_script( 'ecwid-wl-plugins', plugin_dir_url( __FILE__ ) . 'plugins.js' );
	wp_localize_script( 'ecwid-wl-plugins', 'EcwidWLPlugins', array(
		'message' => sprintf( __( 'There is a new version of %s available. <a class="update-link">Update now</a>.' ), ECWID_WL_PLUGIN_NAME )
	));
}
