<?php 
if(!function_exists('barista_edge_accordions_map')) {
    /**
     * Add Accordion options to elements panel
     */
   function barista_edge_accordions_map() {
		
       $panel = barista_edge_add_admin_panel(array(
           'title' => esc_html__('Accordions', 'baristawp' ),
           'name'  => 'panel_accordions',
           'page'  => '_elements_page'
       ));

       //Typography options
       barista_edge_add_admin_section_title(array(
           'name' => 'typography_section_title',
           'title' => esc_html__('Typography', 'baristawp' ),			
           'parent' => $panel
       ));

       $typography_group = barista_edge_add_admin_group(array(
           'name' => 'typography_group',
           'title' => esc_html__('Typography', 'baristawp' ),
			'description' => esc_html__('Setup typography for accordions navigation', 'baristawp' ),
           'parent' => $panel
       ));

       $typography_row = barista_edge_add_admin_row(array(
           'name' => 'typography_row',
           'next' => true,
           'parent' => $typography_group
       ));

       barista_edge_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'fontsimple',
           'name'          => 'accordions_font_family',
           'default_value' => '',
           'label'         => esc_html__('Font Family', 'baristawp' ),
       ));

       barista_edge_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'selectsimple',
           'name'          => 'accordions_text_transform',
           'default_value' => '',
           'label'         => esc_html__('Text Transform', 'baristawp' ),
           'options'       => barista_edge_get_text_transform_array()
       ));

       barista_edge_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'selectsimple',
           'name'          => 'accordions_font_style',
           'default_value' => '',
           'label'         => esc_html__('Font Style', 'baristawp' ),
           'options'       => barista_edge_get_font_style_array()
       ));

       barista_edge_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'textsimple',
           'name'          => 'accordions_letter_spacing',
           'default_value' => '',
           'label'         => esc_html__('Letter Spacing', 'baristawp' ),
           'args'          => array(
               'suffix' => 'px'
           )
       ));

       $typography_row2 = barista_edge_add_admin_row(array(
           'name' => 'typography_row2',
           'next' => true,
           'parent' => $typography_group
       ));		
		
       barista_edge_add_admin_field(array(
           'parent'        => $typography_row2,
           'type'          => 'selectsimple',
           'name'          => 'accordions_font_weight',
           'default_value' => '',
           'label'         => esc_html__('Font Weight', 'baristawp' ),
           'options'       => barista_edge_get_font_weight_array()
       ));
		
		//Initial Accordion Color Styles
		
		barista_edge_add_admin_section_title(array(
           'name' => 'accordion_color_section_title',
           'title' => esc_html__('Basic Accordions Color Styles', 'baristawp' ),			
           'parent' => $panel
       ));
		
		$accordions_color_group = barista_edge_add_admin_group(array(
           'name' => 'accordions_color_group',
           'title' => esc_html__('Accordion Color Styles', 'baristawp' ),
			'description' => esc_html__('Set color styles for accordion title', 'baristawp' ),
           'parent' => $panel
       ));
		$accordions_color_row = barista_edge_add_admin_row(array(
           'name' => 'accordions_color_row',
           'next' => true,
           'parent' => $accordions_color_group
       ));
		barista_edge_add_admin_field(array(
           'parent'        => $accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_title_color',
           'default_value' => '',
           'label'         => esc_html__('Title Color', 'baristawp' )
       ));
		barista_edge_add_admin_field(array(
           'parent'        => $accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_icon_color',
           'default_value' => '',
           'label'         => esc_html__('Icon Color', 'baristawp' )
       ));
		
		$active_accordions_color_group = barista_edge_add_admin_group(array(
           'name' => 'active_accordions_color_group',
           'title' => esc_html__('Active and Hover Accordion Color Styles', 'baristawp' ),
			'description' => esc_html__('Set color styles for active and hover accordions', 'baristawp' ),
           'parent' => $panel
       ));
		$active_accordions_color_row = barista_edge_add_admin_row(array(
           'name' => 'active_accordions_color_row',
           'next' => true,
           'parent' => $active_accordions_color_group
       ));
		barista_edge_add_admin_field(array(
           'parent'        => $active_accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_title_color_active',
           'default_value' => '',
           'label'         => esc_html__('Title Color', 'baristawp' )
       ));
		barista_edge_add_admin_field(array(
           'parent'        => $active_accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_icon_color_active',
           'default_value' => '',
           'label'         => esc_html__('Icon Color', 'baristawp' )
       ));
   }
   add_action('barista_edge_options_elements_map', 'barista_edge_accordions_map');
}