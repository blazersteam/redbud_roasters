<?php

if(!function_exists('barista_edge_button_map')) {
    function barista_edge_button_map() {
        $panel = barista_edge_add_admin_panel(array(
            'title' => esc_html__('Button','baristawp'),
            'name'  => 'panel_button',
            'page'  => '_elements_page'
        ));

        //Typography options
        barista_edge_add_admin_section_title(array(
            'name' => 'typography_section_title',
            'title' => esc_html__('Typography','baristawp'),
            'parent' => $panel
        ));

        $typography_group = barista_edge_add_admin_group(array(
            'name' => 'typography_group',
            'title' => esc_html__('Typography','baristawp'),
            'description' => esc_html__('Setup typography for all button types','baristawp'),
            'parent' => $panel
        ));

        $typography_row = barista_edge_add_admin_row(array(
            'name' => 'typography_row',
            'next' => true,
            'parent' => $typography_group
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'fontsimple',
            'name'          => 'button_font_family',
            'default_value' => '',
            'label'         => esc_html__('Font Family','baristawp'),
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_text_transform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform','baristawp'),
            'options'       => barista_edge_get_text_transform_array()
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_font_style',
            'default_value' => '',
            'label'         => esc_html__('Font Style','baristawp'),
            'options'       => barista_edge_get_font_style_array()
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'textsimple',
            'name'          => 'button_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing','baristawp'),
            'args'          => array(
                'suffix' => 'px'
            )
        ));

        $typography_row2 = barista_edge_add_admin_row(array(
            'name' => 'typography_row2',
            'next' => true,
            'parent' => $typography_group
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $typography_row2,
            'type'          => 'selectsimple',
            'name'          => 'button_font_weight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight','baristawp'),
            'options'       => barista_edge_get_font_weight_array()
        ));

        //Outline type options
        barista_edge_add_admin_section_title(array(
            'name' => 'type_section_title',
            'title' => esc_html__('Types','baristawp'),
            'parent' => $panel
        ));

        $outline_group = barista_edge_add_admin_group(array(
            'name' => 'outline_group',
            'title' => esc_html__('Outline Type','baristawp'),
            'description' => esc_html__('Setup outline button type','baristawp'),
            'parent' => $panel
        ));

        $outline_row = barista_edge_add_admin_row(array(
            'name' => 'outline_row',
            'next' => true,
            'parent' => $outline_group
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color','baristawp'),
            'description'   => ''
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Hover Color','baristawp'),
            'description'   => ''
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Background Color','baristawp'),
            'description'   => ''
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color','baristawp'),
            'description'   => ''
        ));

        $outline_row2 = barista_edge_add_admin_row(array(
            'name' => 'outline_row2',
            'next' => true,
            'parent' => $outline_group
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $outline_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_border_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Border Color','baristawp'),
            'description'   => ''
        ));

	    //Outline Light type options
	    $outline_light_group = barista_edge_add_admin_group(array(
		    'name' => 'outline_light_group',
		    'title' => esc_html__('Outline Light Type','baristawp'),
		    'description' => esc_html__('Setup outline light button type','baristawp'),
		    'parent' => $panel
	    ));

	    $outline_light_row = barista_edge_add_admin_row(array(
		    'name' => 'outline_light_row',
		    'next' => true,
		    'parent' => $outline_light_group
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $outline_light_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_text_color',
		    'default_value' => '',
		    'label'         => esc_html__('Text Color','baristawp'),
		    'description'   => ''
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $outline_light_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_hover_text_color',
		    'default_value' => '',
		    'label'         => esc_html__('Text Hover Color','baristawp'),
		    'description'   => ''
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $outline_light_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_hover_bg_color',
		    'default_value' => '',
		    'label'         => esc_html__('Hover Background Color','baristawp'),
		    'description'   => ''
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $outline_light_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_border_color',
		    'default_value' => '',
		    'label'         => esc_html__('Border Color','baristawp'),
		    'description'   => ''
	    ));

	    $outline_light_row2 = barista_edge_add_admin_row(array(
		    'name' => 'outline_light_row2',
		    'next' => true,
		    'parent' => $outline_light_group
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $outline_light_row2,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_hover_border_color',
		    'default_value' => '',
		    'label'         => esc_html__('Hover Border Color','baristawp'),
		    'description'   => ''
	    ));

        //Solid type options
        $solid_group = barista_edge_add_admin_group(array(
            'name' => 'solid_group',
            'title' => esc_html__('Solid Type','baristawp'),
            'description' => esc_html__('Setup solid button type','baristawp'),
            'parent' => $panel
        ));

        $solid_row = barista_edge_add_admin_row(array(
            'name' => 'solid_row',
            'next' => true,
            'parent' => $solid_group
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color','baristawp'),
            'description'   => ''
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Hover Color','baristawp'),
            'description'   => ''
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Background Color','baristawp'),
            'description'   => ''
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Background Color','baristawp'),
            'description'   => ''
        ));

        $solid_row2 = barista_edge_add_admin_row(array(
            'name' => 'solid_row2',
            'next' => true,
            'parent' => $solid_group
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color','baristawp'),
            'description'   => ''
        ));

        barista_edge_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_border_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Border Color','baristawp'),
            'description'   => ''
        ));

	    //Solid dark type options
	    $solid_dark_group = barista_edge_add_admin_group(array(
		    'name' => 'solid_dark_group',
		    'title' => esc_html__('Solid Dark Type','baristawp'),
		    'description' => esc_html__('Setup Solid Dark button type','baristawp'),
		    'parent' => $panel
	    ));

	    $solid_dark_row = barista_edge_add_admin_row(array(
		    'name' => 'solid_dark_row',
		    'next' => true,
		    'parent' => $solid_dark_group
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_text_color',
		    'default_value' => '',
		    'label'         => esc_html__('Text Color','baristawp'),
		    'description'   => ''
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_hover_text_color',
		    'default_value' => '',
		    'label'         => esc_html__('Text Hover Color','baristawp'),
		    'description'   => ''
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_bg_color',
		    'default_value' => '',
		    'label'         => esc_html__('Background Color','baristawp'),
		    'description'   => ''
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_hover_bg_color',
		    'default_value' => '',
		    'label'         => esc_html__('Hover Background Color','baristawp'),
		    'description'   => ''
	    ));

	    $solid_dark_row2 = barista_edge_add_admin_row(array(
		    'name' => 'solid_dark_row2',
		    'next' => true,
		    'parent' => $solid_dark_group
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row2,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_border_color',
		    'default_value' => '',
		    'label'         => esc_html__('Border Color','baristawp'),
		    'description'   => ''
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row2,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_hover_border_color',
		    'default_value' => '',
		    'label'         => esc_html__('Hover Border Color','baristawp'),
		    'description'   => ''
	    ));

	    //Transparent type options
	    $transparent_group = barista_edge_add_admin_group(array(
		    'name' => 'transparent_group',
		    'title' => esc_html__('Transparent Type','baristawp'),
		    'description' => esc_html__('Setup Transparent button type','baristawp'),
		    'parent' => $panel
	    ));

	    $transparent_row = barista_edge_add_admin_row(array(
		    'name' => 'transparent_row',
		    'next' => true,
		    'parent' => $transparent_group
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $transparent_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_transparent_text_color',
		    'default_value' => '',
		    'label'         => esc_html__('Text Color','baristawp'),
		    'description'   => ''
	    ));

	    barista_edge_add_admin_field(array(
		    'parent'        => $transparent_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_transparent_hover_text_color',
		    'default_value' => '',
		    'label'         => esc_html__('Text Hover Color','baristawp'),
		    'description'   => ''
	    ));
    }

    add_action('barista_edge_options_elements_map', 'barista_edge_button_map');
}

