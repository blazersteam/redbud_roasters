<?php
namespace BaristaEdge\Modules\Shortcodes\ElementsHolder;

use BaristaEdge\Modules\Shortcodes\Lib\ShortcodeInterface;

class ElementsHolder implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'edgtf_elements_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Edge Elements Holder', 'baristawp'),
			'base' => $this->base,
			'icon' => 'icon-wpb-elements-holder extended-custom-icon',
			'category' => esc_html__('by EDGE', 'baristawp'),
			'as_parent' => array('only' => 'edgtf_elements_holder_item'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'colorpicker',
					'class' => '',
					'heading' => esc_html__('Background Color', 'baristawp'),
					'param_name' => 'background_color',
					'value' => '',
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Columns', 'baristawp'),
					'admin_label' => true,
					'param_name' => 'number_of_columns',
					'value' => array(
						esc_html__('1 Column', 'baristawp')      => 'one-column',
						esc_html__('2 Columns', 'baristawp')    	=> 'two-columns',
						esc_html__('3 Columns', 'baristawp')     => 'three-columns',
						esc_html__('4 Columns', 'baristawp')     => 'four-columns',
						esc_html__('5 Columns', 'baristawp')     => 'five-columns',
						esc_html__('6 Columns', 'baristawp')     => 'six-columns'
					),
					'description' => ''
				),
				array(
					'type' => 'checkbox',
					'class' => '',
					'heading' => esc_html__('Items Float Left', 'baristawp'),
					'param_name' => 'items_float_left',
					'value' => array(esc_html__('Make Items Float Left?', 'baristawp') => 'yes'),
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'group' => esc_html__('Width & Responsiveness', 'baristawp'),
					'heading' => esc_html__('Switch to One Column', 'baristawp'),
					'param_name' => 'switch_to_one_column',
					'value' => array(
						esc_html__('Default', 'baristawp')    		=> '',
						esc_html__('Below 1280px', 'baristawp') 	=> '1280',
						esc_html__('Below 1024px', 'baristawp')    	=> '1024',
						esc_html__('Below 768px', 'baristawp')     	=> '768',
						esc_html__('Below 600px', 'baristawp')    	=> '600',
						esc_html__('Below 480px', 'baristawp')    	=> '480',
						esc_html__('Never', 'baristawp')    		=> 'never'
					),
					'description' => esc_html__('Choose on which stage item will be in one column', 'baristawp')
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'group' => esc_html__('Width & Responsiveness', 'baristawp'),
					'heading' => esc_html__('Choose Alignment In Responsive Mode', 'baristawp'),
					'param_name' => 'alignment_one_column',
					'value' => array(
						esc_html__('Default', 'baristawp')    	=> '',
						esc_html__('Left', 'baristawp') 			=> 'left',
						esc_html__('Center', 'baristawp')    	=> 'center',
						esc_html__('Right', 'baristawp')     	=> 'right'
					),
					'description' => esc_html__('Alignment When Items are in One Column', 'baristawp')
				)
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'number_of_columns' 		=> '',
			'switch_to_one_column'		=> '',
			'alignment_one_column' 		=> '',
			'items_float_left' 			=> '',
			'background_color' 			=> ''
		);
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html						= '';

		$elements_holder_classes = array();
		$elements_holder_classes[] = 'edgtf-elements-holder';
		$elements_holder_style = '';

		if($number_of_columns != ''){
			$elements_holder_classes[] .= 'edgtf-'.$number_of_columns ;
		}

		if($switch_to_one_column != ''){
			$elements_holder_classes[] = 'edgtf-responsive-mode-' . $switch_to_one_column ;
		} else {
			$elements_holder_classes[] = 'edgtf-responsive-mode-768' ;
		}

		if($alignment_one_column != ''){
			$elements_holder_classes[] = 'edgtf-one-column-alignment-' . $alignment_one_column ;
		}

		if($items_float_left !== ''){
			$elements_holder_classes[] = 'edgtf-elements-items-float';
		}

		if($background_color != ''){
			$elements_holder_style .= 'background-color:'. $background_color . ';';
		}

		$elements_holder_class = implode(' ', $elements_holder_classes);

		$html .= '<div ' . barista_edge_get_class_attribute($elements_holder_class) . ' ' . barista_edge_get_inline_attr($elements_holder_style, 'style'). '>';
			$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;

	}
}
