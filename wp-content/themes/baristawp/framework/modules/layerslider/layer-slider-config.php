<?php
	if(!function_exists('barista_edge_layerslider_overrides')) {
		/**
		 * Disables Layer Slider auto update box
		 */
		function barista_edge_layerslider_overrides() {
			$GLOBALS['lsAutoUpdateBox'] = false;
		}

		add_action('layerslider_ready', 'barista_edge_layerslider_overrides');
	}
?>